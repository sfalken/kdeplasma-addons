######################################################################
# Automatically generated by qmake (2.01a) Tue Aug 14 08:28:25 2012
######################################################################

TEMPLATE = lib
TARGET = rtm
DEPENDPATH += . tests
INCLUDEPATH += .
DEFINES += QTONLY MAKE_RTM_LIB
QT += network xml
isEmpty(PREFIX) {
  PREFIX=/usr/local
}

# Input
HEADERS += auth.h \
           list.h \
           note.h \
           request.h \
           request_p.h \
           rtm.h \
           rtm_export.h \
           session.h \
           session_p.h \
           task.h \
           task_p.h \
           xmlreaders.h
SOURCES += auth.cpp \
           list.cpp \
           request.cpp \
           session.cpp \
           task.cpp \
           xmlreaders.cpp \

contains(MEEGO_EDITION,harmattan) {
    target.path = $$PREFIX/lib
    INSTALLS += target
}

